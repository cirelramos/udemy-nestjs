import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { CreateMessageDto } from '../dto/create-message-dto';
import { UpdateMessageDto } from '../dto/update-message-dto';
import { MessageService } from '../services/message.service';

@Controller('messages')
export class MessageController {
  constructor(private messageService: MessageService) {}

  @Post()
  create(@Body() CreateMessageDto: CreateMessageDto, @Res() response) {
    this.messageService
      .createMessage(CreateMessageDto)
      .then(message => {
        response.status(HttpStatus.CREATED).json(message);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ message: 'error en la creacion del mensaje' });
      });
  }

  @Get()
  getAll(@Res() response) {
    this.messageService
      .getAll()
      .then(messageList => {
        response.status(HttpStatus.OK).json(messageList);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ message: 'error al obtener la lista de mensajes' });
      });
  }

  @Put(':id')
  update(
    @Body() updateMessageDto: UpdateMessageDto,
    @Res() response,
    @Param('id') idMessage,
  ) {
    this.messageService
      .updateMessage(idMessage, updateMessageDto)
      .then(message => {
        response.status(HttpStatus.OK).json(message);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ message: 'error al editar mensaje' });
      });
  }

  @Delete(':id')
  delete(@Res() response, @Param('id') idMessage) {
    this.messageService
      .deleteMessage(idMessage)
      .then(responseMessage => {
        response.status(HttpStatus.OK).json(responseMessage);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ message: 'error al eliminar message' });
      });
  }
}
