import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Message } from '../entity/message.entity';
import { Repository } from 'typeorm';
import { CreateMessageDto } from '../dto/create-message-dto';
import { UpdateMessageDto } from '../dto/update-message-dto';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private messageRepository: Repository<Message>,
  ) {}

  async getAll(): Promise<object> {
    const data = await this.messageRepository.find();
    const dataNew = data.map(this.mapAddNewAttributeTransform);
    return dataNew;
  }

  mapAddNewAttributeTransform(item, index, array) {
    item['new-value'] = 'data';
    return item;
  }

  async createMessage(messageNew: CreateMessageDto): Promise<Message> {
    const newMessage = new Message();
    newMessage.message = messageNew.message;
    newMessage.nick = messageNew.nick;
    return this.messageRepository.save(newMessage);
  }

  async updateMessage(
    idMessage: number,
    messageToUpdate: UpdateMessageDto,
  ): Promise<Message> {
    const updateMessage = await this.messageRepository.findOne(idMessage);
    updateMessage.message = messageToUpdate.message;
    updateMessage.nick = messageToUpdate.nick;
    return this.messageRepository.save(updateMessage);
  }

  async deleteMessage(idMessage: number): Promise<any> {
    return await this.messageRepository.softDelete(idMessage);
  }
}
