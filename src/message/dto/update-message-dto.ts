export class UpdateMessageDto {
  readonly nick: string;
  readonly message: string;
}
