import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessageController } from './message/controller/message.controller';
import { MessageService } from './message/services/message.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Message } from './message/entity/message.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 2020,
      username: 'root',
      password: 'root',
      database: 'nestjs',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Message])
  ],
  controllers: [AppController, MessageController, MessageController],
  providers: [AppService, MessageService],
})
export class AppModule {
}
